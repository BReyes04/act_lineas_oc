﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Act_Lineas_OC
{
    public partial class Form1 : Form
    {
        public static class Base
        {
            public static string CodBase;
        }
        public static class Compania
        {
            public static string CodCompania;
        }
    
        public static class RowOC
        {
            public static string RowOCCod;
        }
        public static class connetionString
        {
            public static string connetionStringC;
        }
        public static class OC
        {
            public static string CodOc;
        }
        public Form1(string CBase, string Ccia, string row)
        {
            Compania.CodCompania = Ccia;
            RowOC.RowOCCod = row;
            Base.CodBase = CBase;
            InitializeComponent();
            connetionString.connetionStringC = "Data Source=192.168.7.2;Initial Catalog="+Base.CodBase.ToString()+";Persist Security Info=True;User ID=sa;Password=jda";
        }

        private void Form1_Load(object sender, EventArgs e)
        { 
            SqlConnection cnn;
            string sql = null;
            SqlDataAdapter adapter = new SqlDataAdapter();
            cnn = new SqlConnection(connetionString.connetionStringC);
            sql = "SELECT ORDEN_COMPRA FROM " + Compania.CodCompania.ToString() + ".ORDEN_COMPRA WHERE RowPointer='" + RowOC.RowOCCod.ToString() + "';";
            cnn.Open();
            adapter.InsertCommand = new SqlCommand(sql, cnn);
            OC.CodOc = adapter.InsertCommand.ExecuteScalar().ToString();
            label1.Text = "Actulizando Orden de compra "+ OC.CodOc.ToString() + "";
            cargardatagridview();
        }

        public void cargardatagridview()
        {
            //declaramos la cadena  de conexion
            string cadenaconexion = connetionString.connetionStringC.ToString();
            //variable de tipo Sqlconnection
            SqlConnection con = new SqlConnection();
            //variable de tipo Sqlcommand
            SqlCommand comando = new SqlCommand();
            //variable SqlDataReader para leer los datos
            SqlDataReader dr;
            con.ConnectionString = cadenaconexion;
            comando.Connection = con;
            //declaramos el comando para realizar la busqueda
            comando.CommandText = "SELECT ocl.ARTICULO,ocl.U_TIPO_OC FROM " + Compania.CodCompania.ToString() + ".ORDEN_COMPRA_LINEA ocl INNER JOIN  " + Compania.CodCompania.ToString() + ".ORDEN_COMPRA oc ON ocl.ORDEN_COMPRA = oc.ORDEN_COMPRA  WHERE OC.RowPointer='" + RowOC.RowOCCod.ToString() + "';";
            //especificamos que es de tipo Text
            comando.CommandType = CommandType.Text;
            //se abre la conexion
            con.Open();
            //limpiamos los renglones de la datagridview
            dataGridView1.Rows.Clear();
            //a la variable DataReader asignamos  el la variable de tipo SqlCommand
            dr = comando.ExecuteReader();
            //el ciclo while se ejecutará mientras lea registros en la tabla
            while (dr.Read())
            {
                //variable de tipo entero para ir enumerando los la filas del datagridview
                int renglon = dataGridView1.Rows.Add();
                // especificamos en que fila se mostrará cada registro
                // nombredeldatagrid.filas[numerodefila].celdas[nombredelacelda].valor=
                // dr.tipodedatosalmacenado(dr.getordinal(nombredelcampo_en_la_base_de_datos)conviertelo_a_string_sino_es_del_tipo_string);
                dataGridView1.Rows[renglon].Cells["ARTICULO"].Value = dr.GetString(dr.GetOrdinal("ARTICULO"));
                dataGridView1.Rows[renglon].Cells["U_TIPO_OC"].Value = dr.GetString(dr.GetOrdinal("U_TIPO_OC"));
              

            }
            //cierra la conexión
            con.Close();
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            cnn = new SqlConnection(connetionString.connetionStringC);
            string U_Tipo_OC = null;



            try
            {
                if (RndOrden.Checked==true)
                {
                    U_Tipo_OC = "O";
                }
                if (RndStock.Checked == true)
                {
                    U_Tipo_OC = "S";
                }
                cnn.Open();
                string SQL2 = null;
                SQL2 = "UPDATE " + Compania.CodCompania.ToString() + ".ORDEN_COMPRA_LINEA SET U_TIPO_OC='"+ U_Tipo_OC.ToString()+ "' WHERE ORDEN_COMPRA='"+ OC.CodOc.ToString() + "'";
                adapter.UpdateCommand = new SqlCommand(SQL2, cnn);
                adapter.UpdateCommand.ExecuteNonQuery();

                cnn.Close();
                MessageBox.Show("Exito!!!!");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
