﻿namespace Act_Lineas_OC
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ARTICULO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.U_TIPO_OC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.RndStock = new System.Windows.Forms.RadioButton();
            this.RndOrden = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ARTICULO,
            this.U_TIPO_OC});
            this.dataGridView1.Location = new System.Drawing.Point(3, 67);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(801, 386);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ARTICULO
            // 
            this.ARTICULO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ARTICULO.HeaderText = "ARTICULO";
            this.ARTICULO.MinimumWidth = 8;
            this.ARTICULO.Name = "ARTICULO";
            this.ARTICULO.ReadOnly = true;
            // 
            // U_TIPO_OC
            // 
            this.U_TIPO_OC.HeaderText = "U_TIPO_OC";
            this.U_TIPO_OC.MinimumWidth = 8;
            this.U_TIPO_OC.Name = "U_TIPO_OC";
            this.U_TIPO_OC.Width = 150;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // RndStock
            // 
            this.RndStock.AutoSize = true;
            this.RndStock.Location = new System.Drawing.Point(449, 16);
            this.RndStock.Name = "RndStock";
            this.RndStock.Size = new System.Drawing.Size(75, 24);
            this.RndStock.TabIndex = 2;
            this.RndStock.TabStop = true;
            this.RndStock.Text = "Stock";
            this.RndStock.UseVisualStyleBackColor = true;
            // 
            // RndOrden
            // 
            this.RndOrden.AutoSize = true;
            this.RndOrden.Location = new System.Drawing.Point(566, 16);
            this.RndOrden.Name = "RndOrden";
            this.RndOrden.Size = new System.Drawing.Size(78, 24);
            this.RndOrden.TabIndex = 3;
            this.RndOrden.TabStop = true;
            this.RndOrden.Text = "Orden";
            this.RndOrden.UseVisualStyleBackColor = true;
            this.RndOrden.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(657, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 38);
            this.button1.TabIndex = 4;
            this.button1.Text = "Actulizar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.RndOrden);
            this.Controls.Add(this.RndStock);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ARTICULO;
        private System.Windows.Forms.DataGridViewTextBoxColumn U_TIPO_OC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton RndStock;
        private System.Windows.Forms.RadioButton RndOrden;
        private System.Windows.Forms.Button button1;
    }
}

